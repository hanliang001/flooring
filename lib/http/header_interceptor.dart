import 'package:dio/dio.dart';
import 'package:flooring/constant/config.dart';

class HeaderInterceptor extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) {
    options.baseUrl = Config.OAUTH_BASE_URL;
    options.connectTimeout = Config.OAUTH_CONNECT_TIMEOUT;
    return options;
  }
}