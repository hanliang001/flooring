import 'package:dio/dio.dart';
import 'package:flooring/http/header_interceptor.dart';
import 'package:flooring/http/token_interceptor.dart';

class FlooringDio extends Dio {

  factory FlooringDio([BaseOptions options]) => _getInstance(options);
  static FlooringDio _instance;

  FlooringDio._([BaseOptions options]) : super(options) {
    this.interceptors.add(new HeaderInterceptor());
    this.interceptors.add(new TokenInterceptor(this));
  }

  static FlooringDio _getInstance([BaseOptions options]) {
    if (_instance == null) {
      _instance = FlooringDio._(options);
    }
    return _instance;
  }

}

var flooringDio = new FlooringDio();