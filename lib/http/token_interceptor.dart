import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flooring/common/local_storage.dart';
import 'package:flooring/common/string_util.dart';
import 'package:flooring/constant/config.dart';
import 'package:flooring/constant/constant.dart';
import 'package:flooring/model/token.dart';

class TokenInterceptor extends InterceptorsWrapper {
  final Dio _dio;
  final Dio _tokenDio = new Dio(new BaseOptions(baseUrl: Config.OAUTH_BASE_URL));
  String _accessToken;

  TokenInterceptor(this._dio);

  @override
  onRequest(RequestOptions options) async {
    _dio.lock();
    var clientId = "1234";
    var clientSecret = "5678";
    var headValue = clientId + ":" + clientSecret;
    var base64HeadValue = base64.encode(utf8.encode(headValue));
    Options opts = new Options(
        headers: { "Authorization": "Basic " + base64HeadValue },
        contentType: ContentType.parse("application/x-www-form-urlencoded")
    );

    String tokenInfo = await LocalStorage.get(LocalStorage.KEY_TOKEN);
    if (StringUtil.isNullOrEmpty(tokenInfo)) {
      var params = {
        "grant_type": "password",
        "username": Config.OAUTH_USERNAME,
        "password": Config.OAUTH_PASSWORD,
      };
      DateTime startTime = DateTime.now();
      var response = await _tokenDio.post(Constant.API_GET_TOKEN, data: params, options: opts);
      if (response.statusCode == Constant.STATUS_CODE_SUCCESS && Constant.STR_OK == response.statusMessage) {
        Token token = Token.fromJson(response.data);
        token.expirationTime = startTime.add(new Duration(seconds: token.expiresIn));
        LocalStorage.save(LocalStorage.KEY_TOKEN, json.encode(token));
        _accessToken = response.data["access_token"];
      } else {
        return DioError(response: response, message: response.statusMessage);
      }

    } else {
      Token token = Token.fromJson(json.decode(tokenInfo));
      var timeoutDuration = new Duration(milliseconds: Config.OAUTH_CONNECT_TIMEOUT);
      if (token.expirationTime.add(timeoutDuration).compareTo(DateTime.now()) < 0) {
        var params = {
          "grant_type": "refresh_token",
          "refresh_token": token.refreshToken,
        };
        var response = await _tokenDio.post(Constant.API_GET_TOKEN, data: params, options: opts);
        if (response.statusCode == Constant.STATUS_CODE_SUCCESS && Constant.STR_OK == response.statusMessage) {
          LocalStorage.save(LocalStorage.KEY_TOKEN, json.encode(response.data));
          _accessToken = response.data["access_token"];
        } else {
          return DioError(response: response, message: response.statusMessage);
        }
      } else {
        _accessToken = token.accessToken;
      }
    }
    options.headers = { "Authorization": "Bearer " + _accessToken };
    _dio.unlock();
    return super.onRequest(options);
  }
}