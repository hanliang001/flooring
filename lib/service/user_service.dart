import 'dart:convert';
import 'package:flooring/common/user_redux.dart';
import 'package:flooring/http/flooring_dio.dart';

import 'package:dio/dio.dart';
import 'package:flooring/common/local_storage.dart';
import 'package:flooring/common/string_util.dart';
import 'package:flooring/common/theme_redux.dart';
import 'package:flooring/constant/constant.dart';
import 'package:flooring/model/user.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class UserService {
  static initUserInfo(Store store) async {
    String themeIndex = await LocalStorage.get(LocalStorage.KEY_THEME);
    if (!StringUtil.isNullOrEmpty(themeIndex)) {
      var themeData = new ThemeData(primarySwatch: Constant.THEME_COLOR_LIST[int.parse(themeIndex)]);
      store.dispatch(new ThemeDataAction(themeData));
    }

    var userInfo = await LocalStorage.get(LocalStorage.KEY_USER_INFO);
    if (StringUtil.isNullOrEmpty(userInfo)) {
      return false;
    }
    User user = User.fromJson(json.decode(userInfo));
    var result = await login(user.username, user.password, store);
    if (StringUtil.isNullOrEmpty(result)) {
      return true;
    } else {
      return false;
    }
  }

  static login(String username, String password, Store store) async {

    if (StringUtil.isNullOrEmpty(username)) {
      return "Username cannot be empty!";
    }
    if (StringUtil.isNullOrEmpty(password)) {
      return "Password cannot be empty!";
    }

    try {
      var params = {
        "UserName": username,
        "PassWord": password,
      };
      var response = await flooringDio.get(Constant.API_GET_USERNAME_PASSWORD, queryParameters: params);
      if (response.statusCode == Constant.STATUS_CODE_SUCCESS && Constant.STR_OK == response.statusMessage) {
        User user = User.fromJson(response.data);
        user.password = password;
        await LocalStorage.save(LocalStorage.KEY_USERNAME, username);
        await LocalStorage.save(LocalStorage.KEY_PASSWORD, password);
        store.dispatch(new UserAction(user));
        await LocalStorage.save(LocalStorage.KEY_USER_INFO, json.encode(user));
        return Constant.STR_EMPTY;
      } else {
        return "Username and password are not correct!";
      }
    } on DioError catch (e) {
      print(e);
      return e.message;
    }
  }

  static clearAll(Store store) async {
    LocalStorage.remove(LocalStorage.KEY_USER_INFO);
  }
}