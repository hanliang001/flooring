import 'package:flooring/common/global_state.dart';
import 'package:flooring/model/user.dart';
import 'package:flooring/page/home_page.dart';
import 'package:flooring/page/login_page.dart';
import 'package:flooring/page/welcome_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

void main() => runApp(WestlakeFlooringApp());

/// This Widget is the main application widget.
class WestlakeFlooringApp extends StatelessWidget {
  static const String _title = 'Westlake Flooring';

  final store = new Store<GlobalState>(
    appReducer,

    ///初始化数据
    initialState: new GlobalState(
        userInfo: User.empty(),
        themeData: ThemeData(primarySwatch: Colors.blue),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return new StoreProvider(
        store: store,
        child: new StoreBuilder<GlobalState>(
          builder: (context, store) {
            return MaterialApp(
              title: _title,
              theme: store.state.themeData,
              routes: {
                WelcomePage.pageName: (context) {
                  return new WelcomePage();
                },
                HomePage.pageName: (context) {
                  return new HomePage();
                },
                LoginPage.pageName: (context) {
                  return new LoginPage();
                },
              },
            );
          }
        ),
    );
  }
}
