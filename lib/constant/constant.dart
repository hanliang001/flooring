import 'package:flutter/material.dart';

class Constant {
  /// String
  static const STR_EMPTY = "";
  static const STR_OK = "OK";

  /// Web Service Api
  static const API_GET_TOKEN = "/token";
  static const API_GET_USERNAME_PASSWORD = "/api/Login/GetUserNamePassWord";

  /// Status Code
  static const STATUS_CODE_SUCCESS = 200;

  /// Assets Path
  static const ASSET_WELCOME = "assets/images/welcome.jpg";
  static const ASSET_LOGO = "assets/images/westlake-flooring.png";

  /// Theme
  static const THEME_COLOR_LIST = [
    Colors.blue,
    Colors.red,
    Colors.yellow,
    Colors.green,
    Colors.purple,
  ];
}