import 'package:flooring/common/global_state.dart';
import 'package:flooring/common/string_util.dart';
import 'package:flooring/constant/constant.dart';
import 'package:flooring/page/home_page.dart';
import 'package:flooring/service/user_service.dart';
import 'package:flutter/material.dart';
import 'package:flooring/common/local_storage.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  static final String pageName = "login";
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var _username = "";
  var _password = "";

  final TextEditingController usernameController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  _LoginPageState() : super();

  @override
  void initState() {
    super.initState();
    _initParams();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreBuilder<GlobalState>(builder: (context, store) {
      return new GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: new Scaffold(
          body: new Container(
            color: Theme.of(context).primaryColor,
            child: new Center(
              child: new SafeArea(
                child: new SingleChildScrollView(
                  child: new Card(
                    elevation: 5.0,
                    shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    color: Colors.white,
                    margin: const EdgeInsets.only(left: 30.0, right: 30.0),
                    child: new Padding(
                      padding: new EdgeInsets.only(
                          left: 30.0, top: 40.0, right: 30.0, bottom: 0.0),
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Image(
                              image: new AssetImage(Constant.ASSET_LOGO),
                              width: 360.0,
                              height: 144.0,
                              color: Theme.of(context).primaryColor,
                          ),
                          new TextField(
                            controller: usernameController,
                            onChanged: (String value) {
                              _username = value;
                            },
                            decoration: new InputDecoration(
                                hintText: "Username",
                                icon: new Icon(Icons.account_box)),
                          ),
                          new Padding(padding: new EdgeInsets.all(10.0)),
                          new TextField(
                            controller: passwordController,
                            onChanged: (String value) {
                              _password = value;
                            },
                            decoration: new InputDecoration(
                                hintText: "Password",
                                icon: new Icon(Icons.keyboard)),
                            obscureText: true,
                          ),
                          new Padding(padding: new EdgeInsets.all(20.0)),
                          new RaisedButton(
                              padding: new EdgeInsets.only(
                                  left: 20.0,
                                  top: 10.0,
                                  right: 20.0,
                                  bottom: 10.0),
                              textColor: Colors.white,
                              color: Theme.of(context).primaryColor,
                              child: new Flex(
                                mainAxisAlignment: MainAxisAlignment.center,
                                direction: Axis.horizontal,
                                children: <Widget>[
                                  new Expanded(
                                    child: new Text("Login",
                                        style: new TextStyle(fontSize: 20.0),
                                        textAlign: TextAlign.center,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis
                                    ),
                                  )
                                ],
                              ),
                              onPressed: () {
                                UserService.login(_username, _password, store).then((res) {
                                  if (res != null && res.length > 0) {
                                    Fluttertoast.showToast(
                                      msg: res,
                                      gravity: ToastGravity.TOP,
                                      toastLength: Toast.LENGTH_LONG,
                                      backgroundColor: Theme.of(context).primaryColor,
                                    );
                                  } else {
                                    Navigator.pushReplacementNamed(context, HomePage.pageName);
                                  }
                                });
                              }),
                              new Padding(padding: new EdgeInsets.all(30.0)),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    });
  }

  void _initParams() async {
    _username = await LocalStorage.get(LocalStorage.KEY_USERNAME);
    _password = await LocalStorage.get(LocalStorage.KEY_PASSWORD);
    usernameController.value = new TextEditingValue(text: _username ?? "");
    passwordController.value = new TextEditingValue(text: _password ?? "");
  }
}
