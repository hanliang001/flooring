import 'package:flooring/common/global_state.dart';
import 'package:flooring/constant/constant.dart';
import 'package:flooring/page/home_page.dart';
import 'package:flooring/page/login_page.dart';
import 'package:flooring/service/user_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class WelcomePage extends StatefulWidget {
  static final String pageName = "/";

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  bool hadInit = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (hadInit) {
      return;
    }
    hadInit = true;

    ///防止多次进入
    Store<GlobalState> store = StoreProvider.of(context);
    new Future.delayed(const Duration(seconds: 2, milliseconds: 500), () {
      UserService.initUserInfo(store).then((res) {
        if (res != null && res) {
          Navigator.pushReplacementNamed(context, HomePage.pageName);
        } else {
          Navigator.pushReplacementNamed(context, LoginPage.pageName);
        }
        return true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreBuilder<GlobalState>(
      builder: (context, store) {
        return new Image(
          image: new AssetImage(Constant.ASSET_WELCOME),
          fit: BoxFit.fitHeight,
        );
      },
    );
  }
}