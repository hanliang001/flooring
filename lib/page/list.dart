import 'dart:convert';
import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:flooring/page/Detail.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class ListPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new ListState();
  }
}

class ListState extends State<ListPage>{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  var data;

  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return ListView.builder(
        itemCount: data?.length ?? 0,
        itemBuilder: (BuildContext context, int index){
          return Card(
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: ListTile(
                subtitle: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: new Text(
                              data[index]["title"],
                              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text("时间"),
                          new Text(formatDate(DateTime.now() ,[yyyy,'/',mm,'/',dd])),
                        ]
                      ),
                      Row(
                        children: <Widget>[
                          new Container(
                            padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 2.0),
                            child: new Text("id" + data[index]["id"].toString())
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                onTap: () => _onTap(data[index]["id"].toString()),
              ),
            ),
          );
        }
    );

  }

  getData() async {
    var url = 'https://jsonplaceholder.typicode.com/posts';
    var httpclient = new HttpClient();

    var result;
    try{
      var request = await httpclient.getUrl(Uri.parse(url));
      var response = await request.close();
      if(response.statusCode == HttpStatus.ok){
        var json = await response.transform(utf8.decoder).join();
        result = jsonDecode(json);
      }else{
        result = "Error gettting JSON data:\n Http status ${response.statusCode}";
      }
    }catch(exception){
      result = 'Failed getting JSON data';
    }

    if(!mounted) return;

    setState(() {
      data = result;
    });

  }

  void _onTap(String id){
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: false,
        pageBuilder: (BuildContext context, _, __){
          return new Detail(id);
          return null;
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child){
          return new FadeTransition(
            opacity: animation,
            child: new SlideTransition(
                position: new Tween<Offset>(begin: const Offset(0.0, 1.0),end: Offset.zero).animate(animation),
                child: child
            ),
          );
        },
    ));
  }


}