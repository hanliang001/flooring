import 'package:flooring/common/global_state.dart';
import 'package:flooring/page/dashboard_page.dart';
import 'package:flooring/page/lead_page.dart';
import 'package:flooring/page/login_page.dart';
import 'package:flooring/page/me_page.dart';
import 'package:flooring/page/underwriting_page.dart';
import 'package:flooring/service/user_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

class HomePage extends StatefulWidget {
  static final String pageName = "home";
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with SingleTickerProviderStateMixin {

  final PageController _pageController = PageController();

  int _selectedIndex = 0;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreBuilder<GlobalState>(builder: (context, store)
    {
      return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          title: const Text('Westlake Flooring'),
        ),
        body: new PageView(
          controller: _pageController,
          children: [
            new DashboardPage(),
            new LeadPage(),
            new UnderwritingPage(),
            new MePage(),
          ],
          onPageChanged: (index) {
            setState(() {
              _selectedIndex = index;
            });
            _tabController.animateTo(index);
          },
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.dashboard),
              title: Text('Dashboard'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.supervisor_account),
              title: Text('Lead'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.drive_eta),
              title: Text('Underwriting'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Me'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).primaryColor,
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
            });
            _pageController.jumpTo(MediaQuery.of(context).size.width * index);
          },
        ),
      );
    });
  }

}