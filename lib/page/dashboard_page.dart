import 'package:flutter/material.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => new _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  @override
  Widget build(BuildContext context) {
    return new Center(
      child: Text(
        "Dashboard",
        style: TextStyle(fontSize: 30.0),
      ),
    );
  }
}
