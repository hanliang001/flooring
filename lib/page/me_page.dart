import 'dart:convert';

import 'package:flooring/common/global_state.dart';
import 'package:flooring/common/local_storage.dart';
import 'package:flooring/common/string_util.dart';
import 'package:flooring/model/user.dart';
import 'package:flooring/widget/about.dart';
import 'package:flooring/page/login_page.dart';
import 'package:flooring/widget/theme_exchange.dart';
import 'package:flooring/service/user_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class MePage extends StatefulWidget {
  @override
  _MePageState createState() => new _MePageState();
}

class _MePageState extends State<MePage> {
  @override
  Widget build(BuildContext context) {
    return new StoreBuilder<GlobalState>(
      builder: (context, store) {
        return new Padding(
          padding: new EdgeInsets.only(
              left: 0.0, top: 40.0, right: 0.0, bottom: 0.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: new CircleAvatar(
                      radius: 36.0,
                      backgroundColor: Colors.grey[400],
                      backgroundImage: _createUserImage(store.state.userInfo),
                    ),
                  ),
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "${store.state.userInfo.firstName} ${store.state.userInfo.lastName}",
                        style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),
                      ),
                      new Text(
                        store.state.userInfo.email,
                        style: TextStyle(
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              new Padding(padding: new EdgeInsets.all(10.0)),
              new Card(
                margin: EdgeInsets.symmetric(horizontal: 0.0),
                child: new ListTile(
                  title: new Text("Theme"),
                  trailing:
                      new Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                  onTap: () {
                    Navigator.push(context, new MaterialPageRoute(
                      builder: (context) {
                        return new ThemeExchange();
                      },
                    ));
                  },
                ),
              ),
              new Padding(padding: new EdgeInsets.all(3.0)),
              new Card(
                margin: EdgeInsets.symmetric(horizontal: 0.0),
                child: new ListTile(
                  title: new Text("About"),
                  trailing:
                      new Icon(Icons.keyboard_arrow_right, color: Colors.grey),
                  onTap: () {
                    Navigator.push(context, new MaterialPageRoute(
                      builder: (context) {
                        return new About();
                      },
                    ));
                  },
                ),
              ),
              new Padding(padding: new EdgeInsets.all(10.0)),
              new RaisedButton(
                  color: Colors.white,
                  padding: new EdgeInsets.only(top: 15.0, bottom: 15.0),
                  child: new Flex(
                    mainAxisAlignment: MainAxisAlignment.center,
                    direction: Axis.horizontal,
                    children: <Widget>[
                      new Expanded(
                        child: new Text("Logout",
                            style: new TextStyle(fontSize: 16.0),
                            textAlign: TextAlign.center,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      )
                    ],
                  ),
                  onPressed: () {
                    UserService.clearAll(store);
                    LocalStorage.remove(LocalStorage.KEY_TOKEN);
                    Navigator.pushReplacementNamed(context, LoginPage.pageName);
                  }),
            ],
          ),
        );
      },
    );
  }
  
  _createUserImage(User userInfo) {
    if (StringUtil.isNullOrEmpty(userInfo?.userImage)) {
      return null;
    }
    var images = userInfo.userImage.split(',');
    if (images.length < 2) {
      return null;
    }
    return new MemoryImage(base64.decode(images[1]));
  }
 }
