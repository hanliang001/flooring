import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {

  static const KEY_USERNAME = "username";
  static const KEY_PASSWORD = "password";
  static const KEY_USER_INFO = "userinfo";
  static const KEY_THEME = "theme";
  static const KEY_TOKEN = "token";

  static save(String key, value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
  }

  static get(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.get(key);
  }

  static remove(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  static clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }
}
