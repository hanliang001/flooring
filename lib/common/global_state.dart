import 'package:flooring/common/theme_redux.dart';
import 'package:flooring/common/user_redux.dart';
import 'package:flooring/model/user.dart';
import 'package:flutter/material.dart';

class GlobalState {
  User userInfo;
  ThemeData themeData;

  GlobalState({this.userInfo, this.themeData});

}

GlobalState appReducer(GlobalState state, action) {
  return GlobalState(

    ///通过 UserReducer 将 GSYState 内的 userInfo 和 action 关联在一起
    userInfo: userReducer(state.userInfo, action),

    ///通过 ThemeDataReducer 将 GSYState 内的 themeData 和 action 关联在一起
    themeData: themeDataReducer(state.themeData, action),
  );
}