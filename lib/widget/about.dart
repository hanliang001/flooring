import 'package:flutter/material.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About"),
      ),
      body: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Image(
              image: new AssetImage("assets/images/penguin.png"),
              width: 60,
              height: 60,
            ),
            new Padding(padding: new EdgeInsets.all(5.0)),
            new Text("Westlake Flooring Mobile",
                style: new TextStyle(fontSize: 18.0)
            ),
            new Padding(padding: new EdgeInsets.all(5.0)),
            new Text("Version 1.0.0")
          ],
        ),
      )
    );
  }
}
