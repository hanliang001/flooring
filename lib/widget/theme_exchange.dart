import 'package:flooring/common/global_state.dart';
import 'package:flooring/common/local_storage.dart';
import 'package:flooring/common/theme_redux.dart';
import 'package:flooring/constant/constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

class ThemeExchange extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreBuilder<GlobalState>(builder: (context, store) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Theme"),
        ),
        body: new ListView.builder(
          itemCount: Constant.THEME_COLOR_LIST.length,
          itemBuilder: (context, index) {
            return new Card(
              color: Constant.THEME_COLOR_LIST[index],
              child: new ListTile(
                onTap: () {
                  var themeData = new ThemeData(
                      primarySwatch: Constant.THEME_COLOR_LIST[index]);
                  store.dispatch(new ThemeDataAction(themeData));
                  LocalStorage.save(LocalStorage.KEY_THEME, index.toString());
                },
              ),
            );
          },
        ),
      );
    });
  }
}
