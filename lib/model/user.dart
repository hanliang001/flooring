import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  @JsonKey(name: 'userName')
  String username;
  String password;
  String firstName;
  String lastName;
  String email;
  String mobile;
  String phone;
  String fax;
  String userImage;

  User(this.username, this.password, this.firstName, this.lastName, this.email, this.mobile,
      this.phone, this.fax, this.userImage);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  User.empty();

}