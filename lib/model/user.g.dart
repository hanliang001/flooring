// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
      json['userName'] as String,
      json['password'] as String,
      json['firstName'] as String,
      json['lastName'] as String,
      json['email'] as String,
      json['mobile'] as String,
      json['phone'] as String,
      json['fax'] as String,
      json['userImage'] as String);
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'userName': instance.username,
      'password': instance.password,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'email': instance.email,
      'mobile': instance.mobile,
      'phone': instance.phone,
      'fax': instance.fax,
      'userImage': instance.userImage
    };
